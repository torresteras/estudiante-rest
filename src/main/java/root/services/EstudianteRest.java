/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.services;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.model.dao.MasterEstudianteDAO;
import root.model.dao.exceptions.NonexistentEntityException;
import root.persistence.entities.MasterEstudiante;

/**
 *
 * @author alvaro
 */
@Path("Estudiantes")

public class EstudianteRest {

    EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");
    EntityManager em;
    MasterEstudianteDAO dao = new MasterEstudianteDAO();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarTodo() {
        /*  
            Metodo anterior en comentario 
         */
//        em = emf.createEntityManager();
//        List<MasterEstudiante> lista = em.createNamedQuery("MasterEstudiante.findAll").getResultList();
//        return Response.ok(200).entity(lista).build();

        List<MasterEstudiante> lista = dao.findMasterEstudianteEntities();
        System.out.println("Obteniendo datos del dao");
        return Response.ok(200).entity(lista).build();
    }

    @GET
    @Path("/{idbuscar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscarId(@PathParam("idbuscar") String idbuscar) {
        MasterEstudiante estudiante = dao.findMasterEstudiante(idbuscar);
        return Response.ok(200).entity(estudiante).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response nuevo(MasterEstudiante estudianteNuevo) throws Exception {
//        em = emf.createEntityManager();
//        em.getTransaction().begin();
//        em.persist(estudianteNuevo);
//        em.getTransaction().commit();
        MasterEstudianteDAO estudiante = new MasterEstudianteDAO();
        estudiante.create(estudianteNuevo);
        
        
        return Response.ok("Estudiante Creado").build();

    }

    @PUT
    public Response actualizar(MasterEstudiante estudiante) throws Exception {
//        em = emf.createEntityManager();
//        em.getTransaction().begin();
//        estudiante = em.merge(estudiante);
//        em.persist(estudiante);
//        em.getTransaction().commit();
        MasterEstudianteDAO estudiante_1 = new MasterEstudianteDAO();
        estudiante_1.edit(estudiante);
        return Response.ok("Estudiante modificado con exito").build();

    }

    @DELETE
    @Path("/{iddelete}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response eliminaId(@PathParam("iddelete") String iddelete) throws NonexistentEntityException {
//         em= emf.createEntityManager();
//        em.getTransaction().begin();
//        MasterEstudiante estudiante= em.getReference(MasterEstudiante.class, iddelete);
//        em.remove(estudiante);
//        em.getTransaction().commit();
//       return Response.ok("Estudiante eliminado").build();
        MasterEstudiante estudi = new MasterEstudiante();
        MasterEstudianteDAO estudiante = new MasterEstudianteDAO();
        estudiante.destroy(iddelete);
        return Response.ok("Estudiante Eliminado").build();

    }
}
