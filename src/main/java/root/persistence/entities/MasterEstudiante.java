/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.persistence.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author alvaro
 */
@Entity
@Table(name = "master_estudiantes")
@NamedQueries({
    @NamedQuery(name = "MasterEstudiante.findAll", query = "SELECT m FROM MasterEstudiante m"),
    @NamedQuery(name = "MasterEstudiante.findByEdId", query = "SELECT m FROM MasterEstudiante m WHERE m.edId = :edId"),
    @NamedQuery(name = "MasterEstudiante.findByEdNombre", query = "SELECT m FROM MasterEstudiante m WHERE m.edNombre = :edNombre"),
    @NamedQuery(name = "MasterEstudiante.findByEdApellido1", query = "SELECT m FROM MasterEstudiante m WHERE m.edApellido1 = :edApellido1"),
    @NamedQuery(name = "MasterEstudiante.findByEdApellido2", query = "SELECT m FROM MasterEstudiante m WHERE m.edApellido2 = :edApellido2"),
    @NamedQuery(name = "MasterEstudiante.findByEdSeccion", query = "SELECT m FROM MasterEstudiante m WHERE m.edSeccion = :edSeccion"),
    @NamedQuery(name = "MasterEstudiante.findByEdCampus", query = "SELECT m FROM MasterEstudiante m WHERE m.edCampus = :edCampus")})
public class MasterEstudiante implements Serializable {

    @Size(max = 2147483647)
    @Column(name = "ed_nombre")
    private String edNombre;
    @Size(max = 2147483647)
    @Column(name = "ed_apellido1")
    private String edApellido1;
    @Size(max = 2147483647)
    @Column(name = "ed_apellido2")
    private String edApellido2;
    @Size(max = 2147483647)
    @Column(name = "ed_seccion")
    private String edSeccion;
    @Size(max = 2147483647)
    @Column(name = "ed_campus")
    private String edCampus;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "ed_id")
    private String edId;

    public MasterEstudiante() {
    }

    public MasterEstudiante(String edId) {
        this.edId = edId;
    }

    public String getEdId() {
        return edId;
    }

    public void setEdId(String edId) {
        this.edId = edId;
    }

    public String getEdNombre() {
        return edNombre;
    }

    public void setEdNombre(String edNombre) {
        this.edNombre = edNombre;
    }

    public String getEdApellido1() {
        return edApellido1;
    }

    public void setEdApellido1(String edApellido1) {
        this.edApellido1 = edApellido1;
    }

    public String getEdApellido2() {
        return edApellido2;
    }

    public void setEdApellido2(String edApellido2) {
        this.edApellido2 = edApellido2;
    }

    public String getEdSeccion() {
        return edSeccion;
    }

    public void setEdSeccion(String edSeccion) {
        this.edSeccion = edSeccion;
    }

    public String getEdCampus() {
        return edCampus;
    }

    public void setEdCampus(String edCampus) {
        this.edCampus = edCampus;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (edId != null ? edId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MasterEstudiante)) {
            return false;
        }
        MasterEstudiante other = (MasterEstudiante) object;
        if ((this.edId == null && other.edId != null) || (this.edId != null && !this.edId.equals(other.edId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "root.persistence.entities.MasterEstudiante[ edId=" + edId + " ]";
    }

 
}
