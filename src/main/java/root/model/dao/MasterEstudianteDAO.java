/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.model.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import root.model.dao.exceptions.NonexistentEntityException;
import root.model.dao.exceptions.PreexistingEntityException;
import root.persistence.entities.MasterEstudiante;

/**
 *
 * @author alvaro
 */
public class MasterEstudianteDAO implements Serializable {

    public MasterEstudianteDAO(EntityManagerFactory emf) {
        this.emf = emf;
    }

    public MasterEstudianteDAO() {
        this.emf = emf;
    }
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(MasterEstudiante masterEstudiante) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(masterEstudiante);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findMasterEstudiante(masterEstudiante.getEdId()) != null) {
                throw new PreexistingEntityException("MasterEstudiante " + masterEstudiante + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(MasterEstudiante masterEstudiante) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            masterEstudiante = em.merge(masterEstudiante);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = masterEstudiante.getEdId();
                if (findMasterEstudiante(id) == null) {
                    throw new NonexistentEntityException("The masterEstudiante with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            MasterEstudiante masterEstudiante;
            try {
                masterEstudiante = em.getReference(MasterEstudiante.class, id);
                masterEstudiante.getEdId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The masterEstudiante with id " + id + " no longer exists.", enfe);
            }
            em.remove(masterEstudiante);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<MasterEstudiante> findMasterEstudianteEntities() {
        return findMasterEstudianteEntities(true, -1, -1);
    }

    public List<MasterEstudiante> findMasterEstudianteEntities(int maxResults, int firstResult) {
        return findMasterEstudianteEntities(false, maxResults, firstResult);
    }

    private List<MasterEstudiante> findMasterEstudianteEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(MasterEstudiante.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public MasterEstudiante findMasterEstudiante(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(MasterEstudiante.class, id);
        } finally {
            em.close();
        }
    }

    public int getMasterEstudianteCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<MasterEstudiante> rt = cq.from(MasterEstudiante.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
