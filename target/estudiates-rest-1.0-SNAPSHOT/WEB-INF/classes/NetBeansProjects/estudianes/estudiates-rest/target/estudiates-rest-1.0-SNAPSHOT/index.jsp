<%-- 
    Document   : index
    Created on : 03-05-2020, 22:22:52
    Author     : alvaro
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Evaluacion 3 - Alvaro Torres Silva</title>
        <link href="Bootstrap/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="Bootstrap/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="Bootstrap/bootstrap-reboot.min.css" rel="stylesheet" type="text/css"/>
        <link href="Bootstrap/bootstrap-reboot.css" rel="stylesheet" type="text/css"/>
        <link href="Bootstrap/bootstrap-grid.min.css" rel="stylesheet" type="text/css"/>
        <link href="Bootstrap/bootstrap-grid.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="container">
        <h1>Alumno: Alvaro Torres Silva</h1>
        <h1>Seccion: 50</h1>
      
        <p> GET	/estudiates-rest-1.0-SNAPSHOT/api/Estudiantes<br>
            http://192.168.1.101:8080/estudiates-rest-1.0-SNAPSHOT/api/Estudiantes<br>
            *Con este link listamos los estudiante con el metodo GET.
            <br>
            <br>
            POST	/estudiates-rest-1.0-SNAPSHOT/api/Estudiantes<br>
            http://192.168.1.101:8080/estudiates-rest-1.0-SNAPSHOT/api/Estudiantes<br>
            *Con este link creamos un estudiante con el metodo post.
            <br>
            <br>
            PUT	/estudiates-rest-1.0-SNAPSHOT/api/Estudiantes<br>
            http://192.168.1.101:8080/estudiates-rest-1.0-SNAPSHOT/api/Estudiantes<br>
            *Con este link actualizamos el cliente con el metodo PUT.
            <br>
            <br>
            GET	/estudiates-rest-1.0-SNAPSHOT/api/Estudiantes/{idbuscar}<br>
            http://192.168.1.101:8080/estudiates-rest-1.0-SNAPSHOT/api/Estudiantes<br>
            *Con este link Buscamos el estudiante con el ID y el metodo GET.
            <br>
            <br>
            DELETE	/estudiates-rest-1.0-SNAPSHOT/api/Estudiantes/{iddelete}<br>
            http://192.168.1.101:8080/estudiates-rest-1.0-SNAPSHOT/api/Estudiantes/69062288<br>
            *Con este ling eliminamos un estudiante de nuestra bd.
            <br>
            <br>
        </p>
        </table>
        </div>
    </body>
</html>
